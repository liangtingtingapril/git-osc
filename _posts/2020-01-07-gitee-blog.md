---
layout: post
title: "电信你在搞什么？部分电信用户无法正常访问 Gitee 的问题"
---

<p>月初部分使用重庆电信宽带上网的用户反馈无法访问 Gitee ，具体现象是无法解析 gitee.com 域名的 IP 地址，而解析 www.gitee.com 却是正常的。这两天轮到了陕西电信。</p>

<p>之前关于重庆电信的故障分析报告请看<a href="https://www.oschina.net/news/111953/gitee-access-deny-from-chonqqing">这里</a>。</p>

<p>一旦你碰到这种问题时，用户只需要修改本机的 DNS 服务器为一些公共 DNS 服务器即可解决，例如 114.114.114.114 之类，或者用阿里公共DNS、百度公共DNS</p>

<p><img height="358" src="https://static.oschina.net/uploads/space/2020/0107/150948_XxGq_12.png" width="1106" /></p>

<p><img src="http://www.alidns.com/static/img/22.png" /></p>

<p>但是我们也遇见了部分用户无论怎么修改 DNS 服务器都无法生效，因此我们有理由怀疑 53 端口的请求被劫持了。</p>

<p><span style="color:#c0392b"><strong>如果你修改了 DNS 还是无法访问，请一定打电信客服电话进行投诉。</strong></span></p>

<p><img src="https://static.oschina.net/uploads/space/2020/0107/100240_UTKG_12.png" width="400" /></p>

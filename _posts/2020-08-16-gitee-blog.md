---
layout: post
title: "工信部携码云 Gitee 入场，国内开源生态建设进入快车道"
---

<p><span style="color:#393939">2020 </span><span style="color:#393939">年</span><span style="color:#393939">&nbsp;7 </span><span style="color:#393939">月</span><span style="color:#393939">&nbsp;14 </span><span style="color:#393939">日，工业和信息化部技术发展司公布了&ldquo;2</span><span style="color:#393939">020</span><span style="color:#393939">年开源托管平台项目&rdquo;的招标结果，由深圳市奥思网络科技有限公司（开源中国）牵头，与国家工业信息安全发展研究中心、工业和信息化部电子第五研究所、中国电子技术标准化研究院、华为技术有限公司、奇安信科技集团股份有限公司、浪潮电子信息产业股份有限公司、苏州棱镜七彩信息科技有限公司、北京理工大学、西南科技大学共 1</span><span style="color:#393939">0 </span><span style="color:#393939">家单位组成的联合体中标该项目，联合体将依托码云 Gitee</span>&nbsp;<span style="color:#393939">建设中国独立的开源托管平台。</span></p>

<p><span style="color:#393939"><img alt="" height="768" src="https://oscimg.oschina.net/oscnet/up-e2be1ecbeb8e637638c163a4e73c46b451e.png" width="700" /></span></p>

<h4><strong><span style="color:#262626">选择开源，选择未来</span></strong></h4>

<p><span style="color:#393939">2016 </span><span style="color:#393939">年工信部印发的《软件和信息技术服务业发展规划（</span><span style="color:#393939">2016</span><span style="color:#393939">－</span><span style="color:#393939">2020</span><span style="color:#393939">年）》中提到：</span><span style="color:#393939">&quot;</span><span style="color:#393939">发挥开源社区对创新的支撑促进作用，强化开源技术成果在创新中的应用，构建有利于创新的开放式、协作化、国际化开源生态</span><span style="color:#393939">&quot;</span><span style="color:#393939">。</span></p>

<p><span style="color:#393939">此次项目中国家也展现出了开放的心态，并没有选择以政府身份自建，而是对于码云</span><span style="color:#393939">&nbsp;Gitee </span><span style="color:#393939">和民营企业的技术水平表现出充分信任，意味着我国信息技术产业的发展建设已经卓有成效，有了良好的信息技术发展水平，民营企业现在也有能力、有底气去承担国家级项目的责任。</span> </p>

<p><span style="color:#393939">开源产业是未来我国软件和信息技术服务业的持续快速发展的重点，也是不断提升我国信息技术创新水平的重要基础。所以，</span><strong><span style="color:#393939"><strong>发展开源是我国提升信息技术产业水平的必经之路，一个关键的环节和必要的选择。</strong></span></strong></p>

<h4><strong><span style="color:#262626"><strong>为开发者提供更大的平台</strong></span></strong></h4>

<p><span style="color:#393939">作为持续投身国内开源建设的码云</span><span style="color:#393939">&nbsp;Gitee </span><span style="color:#393939">，成立七年来发展迅速，为超过</span><span style="color:#393939">&nbsp;500 </span><span style="color:#393939">万名开发者和</span><span style="color:#393939">&nbsp;10 </span><span style="color:#393939">万家企业提供了服务。平台上托管的开源项目超过了</span><span style="color:#393939">&nbsp;1000 </span><span style="color:#393939">万，其中汇聚了众多国内知名的优秀开源项目，是国内首屈一指的代码托管平台，同时也是世界范围内规模第二大的代码托管平台。</span></p>

<p><span style="color:#393939">如今开源事业获得了国家层面的支持，通过支持码云</span><span style="color:#393939">&nbsp;Gitee </span><span style="color:#393939">来支持国内开发者，为他们提供一个更大的平台施展才华，通过开源来加速创新，国内开源的发展和信息技术产业水平一定能再上一个台阶。</span></p>

<p><span style="color:#393939">码云</span><span style="color:#393939">&nbsp;Gitee </span><span style="color:#393939">也会努力不负国家、社会、合作伙伴们以及广大开发者的信任，将该项目圆满完成。同时也希望更多的企业和开发者加入建设我国开源事业的行列，只有大家共同的参与，我国的开源生态的发展才会越来越蓬勃向上。</span></p>

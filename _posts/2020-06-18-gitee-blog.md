---
layout: post
title: "码云 Gitee 6 月 21 日凌晨维护公告"
---

<p>为了提供更优质的服务，码云 Gitee 将于<strong>2020年6月21日（本周日）凌晨3:00（UTC+8）</strong>对服务器进行硬件升级。</p>

<p>升级期间，会有两次中断，间隔20分钟，每次中断预计5分钟。</p>

<p>请各位用户提前做好安排，给您带来不便，深表歉意！</p>

<p style="text-align:left">Gitee 团队</p>

<p style="text-align:left">2020 年 6&nbsp;月 18&nbsp;日</p>

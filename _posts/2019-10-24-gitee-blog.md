---
layout: post
title: "#1024程序员节# 给码云搜索找茬，获程序员专属礼品"
---

<p>今天 1024 程序员节，来个小活动。码云的搜索刚刚改造完毕（<a href="https://www.oschina.net/news/110781/gitee-search-refine">详情</a>），问题可能还不少，现在我们准备了一些码云周边礼品，将送给发现我们搜索功能硬伤的同学。</p>

<p>时间：2019-10-24<br />
范围：码云的项目搜索（其他的 issue、wiki 等改造还没完成，不在此范畴）</p>

<p>如果你发现该功能的硬伤，请在此文章评论，同一个问题先发者得。请不要提及无关的内容。</p>

<p>问题由&nbsp;<a href="https://my.oschina.net/jcseg">@狮子的魂</a>&nbsp;进行确认，是产品本身考虑不周的问题或者缺陷，并且是可改造的。</p>

<p>礼品有：</p>

<p><img src="https://oscimg.oschina.net/oscnet/24af4c928b0f5a2bd8db2bb90e2c05559da.jpg" /></p>

<p>或者：</p>

<p><img src="https://oscimg.oschina.net/oscnet/546a458723dc120bc93a1195e54079e86c9.jpg" /></p>

<p>Come on ！</p>

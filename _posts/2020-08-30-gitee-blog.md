---
layout: post
title: "感叹国内开源落后？不如和 Gitee 一起改变它"
---

<p>​最近，一些海外媒体，以及国内开发者与网友对 Gitee 中标国家开源托管平台项目的新闻发表了一些看法与讨论，其中一些观点引起了 Gitee 的注意：</p>

<p><img alt="" height="103" src="https://oscimg.oschina.net/oscnet/up-2797596e0e79fe74cdc183a4db19d93acd8.png" width="700" /></p>

<p><span style="color:#7b7f83"><em>「因为两国科技水平的差距，加上 Gitee 依赖于政府，Gitee 不太可能与 GitHub 在同一水平线竞争。」&mdash;&mdash;</em></span><a href="https://www.sixthtone.com/news/1006095/chinese-government-touts-homegrown-alternative-to-github" target="_blank"><em>澎湃新闻英文版「Sixth Tone（第六声）」</em></a></p>

<p><img alt="" height="500" src="https://oscimg.oschina.net/oscnet/up-a001c85dc1dd45f53637845a4fbd319959c.png" width="516" />&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp;</p>

<p><span style="color:#7b7f83"><em>「很难让让开发者群体去接受一个监管下的平台。」&mdash;&mdash;</em></span><em><a href="https://www.sixthtone.com/news/1006095/chinese-government-touts-homegrown-alternative-to-github" target="_blank">澎湃新闻英文版「Sixth Tone（第六声）」</a></em></p>

<p><img alt="" height="190" src="https://oscimg.oschina.net/oscnet/up-2acf193f92578e84452d4f305c5e7fda4e2.png" width="700" />&nbsp;</p>

<p><em>&mdash;&mdash;&nbsp;<a href="https://interconnected.blog/can-you-nationalize-open-source/#chinese-version-below" target="_blank">interconnnected 《能把开源&ldquo;国有化&rdquo;吗？》</a></em></p>

<p>这些观点正在将工信部支持 Gitee 这件事解读成<span style="color:#000000"><strong>「监管」</strong>、<strong>「控制」</strong>、<strong>「国有化」</strong></span>。</p>

<p>我们理解，即使是批评、质疑的声音，也大多是出于对开源的热爱和保护。</p>

<p>但请先厘清几个概念：开源、开源平台、开源平台的支撑资源。</p>

<h3 style="text-align:left">国家的支持意味着什么</h3>

<p style="text-align:left"><span style="color:#494949">的确，美国的科研水平处于世界领先地位，开源文化的发达也是其中一个重要因素。&nbsp;</span></p>

<p>我们的落后是现实，但正因为差距的存在，才更要直面现状、奋起直追。</p>

<p>工信部支持 Gitee 这件事，重点不是 Gitee ，更不是所谓的「打造 GitHub 的备胎」，而是<span style="color:#000000"><strong>通过支持 Gitee 和国内坚持做开源的厂商机构来支持和加速国内开源的发展。</strong>&nbsp;</span></p>

<p>这种支持，是一种<span style="color:#000000"><strong>对技术基础设施建设的支持</strong></span>，而非所谓的「控制」。</p>

<p>开源，从技术圈走入了国家的视野，得到了更多的资源支持，这是一个很好的信号。而热爱开源、有志于建设更好的国内开源生态的同仁们，将把这份支持转化为实实在在的进步。</p>

<h3 style="text-align:left">得到支持后，Gitee 的努力方向&nbsp;</h3>

<p style="text-align:left"><span style="color:#494949">开源平台的运转需要消耗巨大的资源，即便是经过多轮融资的 GitHub，也是在微软收购之后才逐渐取消了对私有库的收费。</span></p>

<p style="text-align:left"><span style="color:#494949">Gitee 从 2013 年开始就一直对私有库免费，一直面临着沉重的成本和资源压力。</span></p>

<p>现在有了更多支持后，Gitee 的使用体验会越来越好，同时也会从产品能力和生态两方面提升，帮助开源项目更好地成长，让更多优秀的国产开源软件被人们所了解。</p>

<blockquote>
<ul>
	<li>Gitee 将持续完善 &nbsp;CI/CD 能力，为开源项目提供便捷强大的 DevOps 服务。同时结合全面的代码质量扫描能力，帮助开发者快速发现问题，提升研发效率。</li>
	<li>Gitee 也会继续增强开源治理方面的功能，包括许可证向导、依赖分析、许可证冲突检测等，帮助开发者了解和合理、合规使用开源项目。</li>
</ul>
</blockquote>

<h3 style="text-align:left">开源平台并不是全部</h3>

<p><span style="color:#494949">我们也应当认识到，开源平台只是整个开源生态之中的一环。</span><span style="color:#000000"><strong><span style="background-color:null">真正成熟和强大的开源生态，离不开一个个优质的开源项目</span></strong></span><span style="color:#494949">&mdash;&mdash;这不是一个功能多么强大、计算资源多么丰富的代码托管平台可以解决的问题。&nbsp;</span></p>

<p><span style="color:#494949">但目前发端于国内的优质开源项目，在数量、质量方面都与国外（特别是美国）有一定差距，这是事实和现状。</span></p>

<p><span style="color:#494949">而未来将会如何，取决于我们每一个人。</span></p>

<p><span style="color:#494949">每个开发者都从开源中受益，而在感叹「国内开源落后」的同时，</span><span style="color:#000000"><strong>是否想过自己可以做一些什么？</strong></span></p>

<p><span style="color:#494949">一个优秀的开源项目，需要经过漫长的迭代才能逐渐成熟，这个过程离不开贡献者的共同参与和努力，以及开发者的包容和支持。</span></p>

<p><span style="color:#494949">我们需要的，是更加深刻的改变：</span></p>

<blockquote>
<ul>
	<li>
	<p>不再习惯性地把国内开源生态的落后归咎于开源文化和氛围，我们应意识到每人都是开源生态中的一员，我们拥有实际行动的力量，良好的开源文化和氛围需要大家的共同努力。</p>
	</li>
	<li>
	<p>不再把开源当作「别人的事」、「大佬们的事」，尽己所能地参与建设，哪怕是一个 Issue、一个 PR，健康的开源生态就是由这一个个反馈、一次次协作构建起来的。</p>
	</li>
</ul>
</blockquote>

<p style="text-align:left">开源生态建设，并非朝夕之功，更是一个聚沙成塔的过程。我们相信中国开发者的创造力，相信坚持的力量。愿我们一起，多为开源做些实事。</p>

<p>临渊羡鱼，不如退而结网。</p>

<p>与其羡慕国外优秀的开源文化和开源项目，不如从我开始、从现在开始，让改变发生、让进步发生。</p>

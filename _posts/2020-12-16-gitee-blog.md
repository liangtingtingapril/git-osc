---
layout: post
title: "基于 Gitee Go 的 Golang 持续集成部署体验"
---

<p>Gitee Go&nbsp;是&nbsp;Gitee&nbsp;推出的 CI/CD 服务，通过自定义构建流程，可以实现从代码仓库到构建部署自动化。目前已支持&nbsp;<code>Maven</code>、<code>Gradle</code>、<code>npm</code>、<code>Python</code>、<code>Ant</code>、<code>PHP</code>、<code>Golang</code>&nbsp;等工具和语言的持续构建与集成能力。</p>
